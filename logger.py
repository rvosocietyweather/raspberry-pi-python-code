import json
import requests
import time
#from socketIO_client_nexus import SocketIO, LogginNamespace
from TSL2561 import getInfraredLuxAmount, getVisibleLuxAmount
from BMP180 import getTemperature, getPressure
from analogTemperature import getAnalogTemperature
#from DHT import temperatuur, luchtvochtigheid
from anemometer import windsnelheid
import HPMA115S0_lib as HPMA115S0

host = ""
apitoken = ""

hpma115S0 = HPMA115S0.HPMA115S0("/dev/ttyS0")

hpma115S0.init()
hpma115S0.startParticleMeasurement()

while True:
            if (hpma115S0.readParticleMeasurement()):
                data = {
                    "token": apitoken,
                    "data": {
                        "analogTemperature": getAnalogTemperature(),
                        "digitalTemperature": getTemperature(),
                        "visibleLuxAmount": getVisibleLuxAmount(),
                        "infraredLuxAmount": getInfraredLuxAmount(),
                        #"humidity": luchtvochtigheid(),
                        "airquality_2_5": hpma115S0._pm2_5,
                        "airquality_10": hpma115S0._pm10,
                        #"windsnelheid": windsnelheid(),
                    }
                }
            
                requests.post(host + "/api/post", json=data)
                
                print('Informatie opgeslagen!')

                time.sleep(60)
                
                #gelukt = true
            else:
                time.sleep(1)
