# Distributed with a free-will license.
# Use it any way you want, profit or free, provided it fits in the licenses of its associated works.
# TSL2561
# This code is designed to work with the TSL2561_I2CS I2C Mini Module available from ControlEverything.com.
# https://www.controleverything.com/content/Light?sku=TSL2561_I2CS#tabs-0-product_tabset-2
# Modified by Sigfried Seldeslachts

import smbus
import time

# Get I2C bus
bus = smbus.SMBus(1)

bus.write_byte_data(0x39, 0x00 | 0x80, 0x03)
bus.write_byte_data(0x39, 0x01 | 0x80, 0x02)

def getInfraredLuxAmount():
    data = bus.read_i2c_block_data(0x39, 0x0E | 0x80, 2)
    
    # Zet de informatie om naar lux en return het
    return data[1] * 256 + data[0]

def getVisibleLuxAmount():
    infraredLuxAmount = getInfraredLuxAmount()

    data = bus.read_i2c_block_data(0x39, 0x0C | 0x80, 2)
    totalLuxAmount = data[1] * 256 + data[0]

    return totalLuxAmount - infraredLuxAmount
