import BMP_lib as BMP

temperatuurIjking = 24 # graden C
drukIjking = 9 # hPa

# BMP180 sensor
bmp180 = BMP.BMP085()

def getTemperature():
    return bmp180.read_temperature() - temperatuurIjking # graden C

def getPressure():
    return bmp180.read_pressure() / 100 - drukIjking # hPa
