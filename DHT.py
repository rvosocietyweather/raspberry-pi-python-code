# Dit bestand moet een verbetering krijgen i.v.m het opvragen van de temperatuur wat zeer lang duurt
# en dus niet 2 keer moet worden uitgevoerd

import time
import DHT_lib as DHT

# Sensortypes (zie voorkant sensor):
# DH11 = DHT.DHT11
# DH22 = DHT.DHT22
# AM2302 = DHT.AM2302
sensorType = DHT.AM2302
GPIO_PIN = 14

def temperatuur():
    # read_retry zal 15 keer proberen de waarden te vragen met telkens 2 seconden er tussen
    luchtvochtigheid, temperatuur = DHT.read_retry(sensorType, GPIO_PIN)
    
    return temperatuur

def luchtvochtigheid():
    # read_retry zal 15 keer proberen de waarden te vragen met telkens 2 seconden er tussen
    luchtvochtigheid, temperatuur = DHT.read_retry(sensorType, GPIO_PIN)
    
    return luchtvochtigheid

