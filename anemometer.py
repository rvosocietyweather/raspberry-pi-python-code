import mcp3008
import time

kanaal = 1 # Dit is pin 2 van de chip

def windsnelheid():
    spanning = round(round(((mcp3008.readChannel(kanaal) * 3300) / 1024), 4) / 1000, 2)

    snelheid = round((spanning - 0.4) * 32.4 / (2 - 0.4), 2)

    return snelheid # m/s
