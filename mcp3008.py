import time
import SPI_lib as SPI
import MCP3008_lib as Adafruit_MCP3008

# Openzetten van SPI bus
spi_poort = 0 # De MCP3008 IC is verbonden met SPI0 op de RPi
spi_apparaat = 0
mcp3008 = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(spi_poort, spi_apparaat))

# Kanaal lezen van MCP3008
def readChannel(channel):
    return mcp3008.read_adc(channel)
