from mcp3008 import readChannel

kanaal = 0 # Dit is pin 1 van de chip

def getAnalogTemperature():
    spanning = round((readChannel(kanaal) * 3300) / 1024, 2)
    
    return round((spanning -500) / 10, 2)
